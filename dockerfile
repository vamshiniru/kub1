FROM openjdk:8-jdk-alpine
RUN apk --no-cache add curl
VOLUME /tmp
COPY  /target/simple-mvn-project-with-tests-2.0-SNAPSHOT.jar app.jar
